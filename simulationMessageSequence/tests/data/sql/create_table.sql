CREATE TABLE IF NOT EXISTS `LearningRun` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningVersionUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningConfigUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `isPublished` tinyint(1) NOT NULL DEFAULT '0',
  `runType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `executionStatus` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `executionDateTime` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningRunUID`),
  KEY `learningRun_fk_3_idx` (`learningBuildUID`),
  CONSTRAINT `learningRun_fk_3` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `SimulationMessageSequence`;
CREATE TABLE `SimulationMessageSequence`(
  `messageSeqUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageSeqHash` char(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL DEFAULT '',
  `messageID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequenceOrder` int(11) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `learningBuildUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `learningRunUID` varchar(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  KEY `SimulationMessageSequence_hash_index` (`messageSeqHash`),
  KEY `SimulationMessageSequence_fk_1` (`learningBuildUID`),
  KEY `SimulationMessageSequence_fk_2` (`learningRunUID`),
  CONSTRAINT `SimulationMessageSequence_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationMessageSequence_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `SimulationSegment`;
CREATE TABLE `SimulationSegment` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentUID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `segmentValue` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`segmentUID`),
  KEY `SimulationSegment_fk_1` (`learningBuildUID`),
  KEY `SimulationSegment_fk_2` (`learningRunUID`),
  CONSTRAINT `SimulationSegment_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationSegment_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `SimulationAccountSegment`;
CREATE TABLE `SimulationAccountSegment` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accountUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accountName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningBuildUID`,`segmentUID`,`accountUID`),
  KEY `SimulationAccountSegment_idx` (`learningBuildUID`,`accountUID`),
  KEY `SimulationAccountSegment_fk_2` (`learningRunUID`),
  CONSTRAINT `SimulationAccountSegment_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationAccountSegment_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



DROP TABLE IF EXISTS `SimulationAccountMessageSequence`;
CREATE TABLE `SimulationAccountMessageSequence` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `accountUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `messageSeqUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `probability` double DEFAULT NULL,
  `isFinalized` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accountName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  KEY `learningBuildUID_idx` (`learningBuildUID`),
  KEY `messageSeqID_idx` (`messageSeqUID`),
  KEY `SimulationAccountMessageSequence_fk_2` (`learningRunUID`),
  CONSTRAINT `SimulationAccountMessageSequence_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationAccountMessageSequence_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `SimulationAccountSentEmail`;
CREATE TABLE `SimulationAccountSentEmail` (
  `InteractionId` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `messageUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isOpen` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clickCount` bigint(20) DEFAULT NULL,
  `emailName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailSubject` mediumtext COLLATE utf8_unicode_ci,
  `emailBody` mediumtext COLLATE utf8_unicode_ci,
  `emailSentDate` datetime DEFAULT NULL,
  `senderEmailID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountEmailID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailLastOpenedDate` datetime DEFAULT NULL,
  `lastClickDate` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  KEY `SimulationAccountSentEmail_idx_2` (`messageUID`,`accountUID`),
  KEY `SimulationAccountSentEmail_idx_1` (`accountUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `SimulationSegmentMessageSequence`;
CREATE TABLE `SimulationSegmentMessageSequence` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `segmentUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `messageSeqUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `probability` double DEFAULT NULL,
  `isFinalized` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `learningBuildUID_idx` (`learningBuildUID`),
  KEY `messageSeqID_idx` (`messageSeqUID`),
  KEY `SimulationSegmentMessageSequence_fk_2` (`learningRunUID`),
  CONSTRAINT `SimulationSegmentMessageSequence_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationSegmentMessageSequence_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `h2oDockerInfo`;
CREATE TABLE `h2oDockerInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0.0.0.0',
  `port` int(11) NOT NULL DEFAULT '54321',
  `status` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `Sent_Email_vod__c_arc` (
  `Id` char(18) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `OwnerId` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `Name` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RecordTypeId` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedById` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL,
  `LastModifiedById` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SystemModstamp` datetime DEFAULT NULL,
  `LastActivityDate` date DEFAULT NULL,
  `MayEdit` tinyint(1) DEFAULT NULL,
  `IsLocked` tinyint(1) DEFAULT NULL,
  `ConnectionReceivedId` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ConnectionSentId` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Account_Email_vod__c` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Account_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Approved_Email_Template_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Capture_Datetime_vod__c` datetime DEFAULT NULL,
  `Detail_Group_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email_Config_Values_vod__c` mediumtext COLLATE utf8_unicode_ci,
  `Email_Content2_vod__c` mediumtext COLLATE utf8_unicode_ci,
  `Email_Content_vod__c` mediumtext COLLATE utf8_unicode_ci,
  `Email_Fragments_vod__c` mediumtext COLLATE utf8_unicode_ci,
  `Email_Sent_Date_vod__c` datetime DEFAULT NULL,
  `Failure_Msg_vod__c` text COLLATE utf8_unicode_ci,
  `Last_Activity_Date_vod__c` datetime DEFAULT NULL,
  `Last_Device_vod__c` text COLLATE utf8_unicode_ci,
  `MC_Capture_Datetime_vod__c` datetime DEFAULT NULL,
  `Mobile_ID_vod__c` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Opened_vod__c` bigint(20) DEFAULT NULL,
  `Product_Display_vod__c` text COLLATE utf8_unicode_ci,
  `Product_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Sender_Email_vod__c` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status_vod__c` text COLLATE utf8_unicode_ci,
  `Valid_Consent_Exists_vod__c` tinyint(1) DEFAULT NULL,
  `Approved_Document_Views_vod__c` bigint(20) DEFAULT NULL,
  `Click_Count_vod__c` bigint(20) DEFAULT NULL,
  `Last_Click_Date_vod__c` datetime DEFAULT NULL,
  `Last_Open_Date_vod__c` datetime DEFAULT NULL,
  `Territory_vod__c` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Open_Count_vod__c` bigint(20) DEFAULT NULL,
  `Call2_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Related_Transaction_ID_vod__c` longtext COLLATE utf8_unicode_ci,
  `User_vod__c` char(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arcstartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arcendDate` datetime DEFAULT NULL,
  `scdRowTypeId` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `dateOfProcessRun` datetime NOT NULL,
  `Email_Source_vod__c` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`,`arcstartDate`),
  KEY `Id` (`Id`),
  KEY `arcendDate` (`arcendDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

