# context('testing rebuildModel() func in MSO module')
# print(Sys.time())
#
# library(RMySQL)
# library(openxlsx)
# library(properties)
# library(uuid)
# source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
# source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
#
# # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
# requiredMockDataList <- list(c('Product','Facility','RepTeam','Message','MessageSetMessage','StrategyTarget','TargetingLevel','RepActionType','TargetsPeriod','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account','MessageAlgorithm','MessageSet'),c('LearningRun','LearningFile','LearningBuild'),c('AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),c('Approved_Document_vod__c')) # requiredMockDatalist in order of c('pfizerusdev','pfizerusdev_learning','pfizerusdev_stage','pfizerprod_cs')
# resetMockData(dbuser,dbhost,port,dbpassword,dbname_filepath_match,dbname_cs,requiredMockDataList)
#
# # setup build directory
# buildUID <- readModuleConfig(homedir, 'messageSequence','buildUID')
# # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
# setupMockBuildDir(homedir, 'messageSequence', buildUID, forNightly=TRUE)
# # get number of files in build directory ow
# numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))
#
# # prepare entry in learningBuild
# # add entry to learningBuild (simulate what already there when last build)
# propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID);
# config <- read.properties(propertiesFilePath)
# VERSION_UID <- config[["versionUID"]]
# CONFIG_UID <- config[["configUID"]]
# drv <- dbDriver("MySQL")
# con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
# now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
# SQL <- sprintf("INSERT INTO LearningBuild (learningBuildUID,learningVersionUID,learningConfigUID,isDeployed,executionStatus,executionDatetime,isDeleted) VALUES('%s','%s','%s',1,'failure','%s',0);",buildUID, VERSION_UID, CONFIG_UID, now)
# dbClearResult(dbSendQuery(con,SQL))
# # add entry to LearningFile to simulate where already there wehn last build (for a fail build)
# runUID <- UUIDgenerate()
# SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data,createdAt) VALUES('%s',NULL,'%s','%s','%s',0,NULL,'%s');",UUIDgenerate(),buildUID,"log","txt",now)
# dbClearResult(dbSendQuery(con,SQL))
# SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data,createdAt) VALUES('%s',NULL,'%s','%s','%s',0,NULL,'%s');",UUIDgenerate(),buildUID,"print","txt",now)
# dbClearResult(dbSendQuery(con,SQL))
# # disconnect DB
# dbDisconnect(con)
#
# # run script
# targetNames <<- c('a3RA00000001MtAMAU', 'a3RA0000000e0u5MAA', 'a3RA0000000e0wBMAQ', 'a3RA0000000e486MAA', 'a3RA0000000e47gMAA') # force to run on build the messages (without this param also works)
# source(sprintf('%s/messageSequence/rebuildModel.r',homedir))
# # source will output runUID
#
# # test cases
# # test build directory structure
# test_that("build dir has correct struture", {
#   expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
#   expect_num_of_file(sprintf('%s/builds/%s',homedir,buildUID), numOfFilesInBuildFolder+3)
#   expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,buildUID,RUN_UID))
#   expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,buildUID,RUN_UID))
#   expect_file_exists(sprintf('%s/builds/%s/plot_output_%s_%s',homedir,buildUID,buildUID,RUN_UID))
#   expect_file_exists(sprintf('%s/builds/%s/models_%s',homedir,buildUID,buildUID))
#   expect_file_exists(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,buildUID,buildUID))
#   expect_num_of_file(sprintf('%s/builds/%s/models_%s',homedir,buildUID,buildUID),4)
# })
#
# # test model excel file
# test_that("model reference saved in excel is correct", {
#   # read excel sheet 2 - importance file
#   importance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,buildUID, buildUID),sheet=2)
#   expect_equal(dim(importance),c(562,6))
#
#   # read excel sheet 3 - accuracy file
#   models <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,buildUID, buildUID),sheet=3)
#   expect_equal(dim(models),c(2,14))
#   for (file in models$modelName)
#   {expect_file_exists(file)}
#
#   # read excel sheet 4 - aggImportance file
#   aggImportance <- read.xlsx(sprintf('%s/builds/%s/messageSequenceDriver.r_%s.xlsx',homedir,BUILD_UID, BUILD_UID),sheet=4)
#   expect_equal(dim(aggImportance),c(71,8))
# })
#
# # test DB entry
# test_that("correct entry write to learning DB", {
#   drv <- dbDriver("MySQL")
#   con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname_learning,port=port,password=dbpassword)
#   learningRun <- dbGetQuery(con, "SELECT * from LearningRun;")
#   SQL <- sprintf("SELECT * from LearningBuild where learningBuildUID='%s';",buildUID)
#   learningBuild <- dbGetQuery(con, SQL)
#   SQL <- sprintf("SELECT * from LearningFile where learningBuildUID='%s';",buildUID)
#   learningFile <- dbGetQuery(con, SQL)
#   dbDisconnect(con)
#   expect_equal(dim(learningRun), c(0,9))
#   expect_equal(dim(learningBuild), c(1,9))
#   expect_equal(unname(unlist(learningBuild[,c('isDeployed','executionStatus','isDeleted')])), c(1,'success',0))
#   expect_equal(learningBuild$updatedAt>learningBuild$createdAt, TRUE)
#   expect_equal(learningBuild$executionDatetime>learningBuild$createdAt, TRUE)
#   expect_equal(dim(learningFile), c(7,9))
#   expect_setequal(learningFile$fileName, c("accuracy", "importance", "log", "print", "log", "print", "aggImportance"))
#   expect_equal(sum(learningFile$createdAt>now), 5) # five entry are all new insert
# })
