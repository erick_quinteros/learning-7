##########################################################
#
#
# aktana- messageSequence cleanup of server artifacts
#
# description: driver pgm
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2017.
#
#
####################################################################################################################
library(RMySQL)
library(data.table)


options(rgl.useNULL=TRUE)

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
    print("Arguments supplied.")
    for(i in 1:length(args)){
       eval(parse(text=args[[i]]))
       print(args[[i]]);
    }
}

# read algorithm table and prepare to loop through all active models
drv <- dbDriver("MySQL")
con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname)
modelsToKeep <- data.table(dbGetQuery(con,"Select messageAlgorithmId, externalId from MessageAlgorithm;"))
dbDisconnect(con)
keep <- modelsToKeep$externalId
keep <- paste("Run_",keep,sep="")
print("directories to keep")
print(keep)

# create job
dirloc <- sprintf("%s/../data",homedir)
setwd(dirloc)
dir <- list.files(".")
#dir <- dir[3:(length(dir))]  # also remove configurations and catalog.txt
print(dir)
delete <- dir[!(dir %in% keep)]

print("directories to delete")
print(delete)

#delete the directories
unlink(delete,recursive=T,force=T)
print("final list of directories")
print(list.files("."))

# create the configurations directory
dir.create(sprintf("%s/../data/Configurations",homedir))

# create the catalog.txt file
write(" ",sprintf("%s/../data/Catalog.txt",homedir))
