context('testing processAnchorResult.Facility, processAnchorResult.Centroid func defined in processAnchorResult script in ANCHOR module')
print(Sys.time())

# load library and source script
library(Learning)
library(properties)
library(data.table)
source(sprintf("%s/anchor/code/utils.r",homedir))
source(sprintf("%s/anchor/code/processAnchorResult.r",homedir))

# get required input from learning.properties
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
predictRundate <- getConfigurationValueNew(config,"LE_AN_manualPredictRundate",convertFunc=as.Date)

# load required input to function
load(sprintf('%s/anchor/tests/data/from_calculateAnchor_result.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep.RData', homedir))

# run tests
test_that("test processAnchorResult.Centroid ", {
  result_centroid_new <- processAnchorResult.Centroid(rbind(result,resultNewRep), predictRundate)
  # after validating that new data is correct, uncomment to update unit test data
  # result_centroid <- result_centroid_new
  # save(result_centroid, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_centroid.RData', homedir))
  expect_equal(dim(result_centroid_new), c(15,9))
  expect_equal(result_centroid_new, result_centroid)
})


test_that("processAnchorResult.Facility for manual run", {
  isNightly <- FALSE
  result_facility_new <- processAnchorResult.Facility(rbind(result,resultNewRep), predictRundate, isNightly)
  # after validating that new data is correct, uncomment to update unit test data
  # result_facility <- result_facility_new
  # save(result_facility, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility.RData', homedir))
  expect_equal(dim(result_facility_new), c(463,9))
  expect_equal(result_facility_new, result_facility)
})

test_that("processAnchorResult.Facility for nightly run", {
  isNightly <- TRUE
  result_facility_new <- processAnchorResult.Facility(rbind(result,resultNewRep), predictRundate, isNightly)
  # after validating that new data is correct, uncomment to update unit test data
  # result_facility_DSE <- result_facility_new
  # save(result_facility_DSE, file=sprintf('%s/anchor/tests/data/from_processAnchorResult_facility_DSE.RData', homedir))
  load(sprintf('%s/anchor/tests/data/from_processAnchorResult_facility_DSE.RData', homedir))
  expect_equal(dim(result_facility_new), c(391,9))
  expect_equal(result_facility_new, result_facility_DSE)
})


