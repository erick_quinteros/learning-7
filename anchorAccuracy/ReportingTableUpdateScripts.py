# import pandas as pd
# import numpy as np
# import warnings
# from datetime import datetime, timedelta

from common.pyUtils.logger import get_module_logger
from common.pyUtils.database_config import DatabaseConfig
from common.pyUtils.database_utils import other_database_op

logger = get_module_logger(__name__)

# change data sources easily
tablename_RPT_RepDateLocation_history = "RPT_RepDateLocation_history"
tablename_RPT_RepDateLocation_history_TEMP_COPY = "RPT_RepDateLocation_history_TEMP_COPY"
tablename_RPT_RepDateLocationMap_history = "RPT_RepDateLocationMap_history"
tablename_RPT_Degrees = "RPT_Degrees"
tablename_RepDateLocation_arc = "RepDateLocation_arc"
tablename_RepDateLocation = "RepDateLocation"
tablename_Rep = "Rep"
tablename_RepLocation = "RepLocation"

class ReportingTableUpdateScripts:
    """
    object to handle accuracy report for nightly run
    """

    def __init__(self, conn_pool):
        self.dbconfig = DatabaseConfig.instance()
        self.conn_pool = conn_pool
        self.tablename_dict = {}
        self.empty_table_set = set([])
        self._update_table_names()
        self._check_for_empty_tables()


    def _update_table_names(self):
        self.tablename_dict["tablename_RPT_RepDateLocation_history"] = self.dbconfig.dbname_learning + "." + tablename_RPT_RepDateLocation_history
        self.tablename_dict["tablename_RPT_RepDateLocation_history_TEMP_COPY"] = self.dbconfig.dbname_learning + "." + tablename_RPT_RepDateLocation_history_TEMP_COPY
        self.tablename_dict["tablename_RPT_RepDateLocationMap_history"] = self.dbconfig.dbname_learning + "." + tablename_RPT_RepDateLocationMap_history
        self.tablename_dict["tablename_RPT_Degrees"] = self.dbconfig.dbname_learning + "." + tablename_RPT_Degrees
        self.tablename_dict["tablename_RepDateLocation_arc"] = self.dbconfig.dbname_stage + "." + tablename_RepDateLocation_arc
        self.tablename_dict["tablename_RepDateLocation"] = self.dbconfig.dbname_dse + "." + tablename_RepDateLocation
        self.tablename_dict["tablename_Rep"] = self.dbconfig.dbname_dse + "." + tablename_Rep
        self.tablename_dict["tablename_RepLocation"] = self.dbconfig.dbname_dse + "." + tablename_RepLocation

    def _check_for_empty_tables(self):
        for key in self.tablename_dict:
            try:
                return_cursor = other_database_op("SELECT EXISTS (SELECT 1 FROM {tableName});".format(tableName=self.tablename_dict[key]), self.conn_pool, return_cursor=True)
                if return_cursor.fetchone()[0] == 0: self.empty_table_set.add(key)
            except Exception: # table may not exist
                self.empty_table_set.add(key)
        logger.info(self.empty_table_set)

    def _update_RPT_RepDateLocation_history(self):
        logger.info("updating RPT_RepDateLocation_history")
        if "tablename_RPT_RepDateLocation_history" in self.empty_table_set and "tablename_RepDateLocation_arc" not in self.empty_table_set:
            logger.info("RPT_RepDateLocation_history is empty and RepDateLocation_arc not empty, doing initial insert")
            initial_insert = \
                '''INSERT INTO {tablename_RPT_RepDateLocation_history}
                (repId,`date`,latitude,longitude,latLongString,maxNearDistance,maxFarDistance,dailyTravelDistanceMi,dailyTravelDistanceKm,learningRunUID,source,sourceCatagory,repTypeId, workWeekId,externalId,repName,timeZoneId,isActivated,isDeleted,seConfigId,`blend`)
                SELECT
                    A.repId,
                    A.date,
                    A.latitude,
                    A.longitude,
                    CONCAT("Lat: ", ROUND(A.Latitude, 2), ", Long: ", ROUND(A.Longitude, 2)),
                    IFNULL(IFNULL(A.maxNearDistance, C.avgMilesCurrentAccounts * 1609.34), 0),
                    IFNULL(IFNULL(A.maxFarDistance, C.maxMilesCurrentAccounts * 1609.34), 0),
                    ROUND(IFNULL(IFNULL(A.maxNearDistance, C.avgMilesCurrentAccounts * 1609.34), 0) / 1609.34, 2),
                    ROUND(IFNULL(IFNULL(A.maxNearDistance, C.avgMilesCurrentAccounts * 1609.34), 0) / 1000, 2),
                    A.learningRunUID,
                    A.source,
                    case
  			            when A.source regexp 'calendar' then 'calendar'
  			            when A.source regexp 'history' OR A.source is null then 'history'
  			            when A.source regexp 'territory' then 'territory'  
			        else 
  				        A.source
			        end,
                    B.repTypeId,
                    B.workWeekId,
                    B.externalId,
                    B.repName,
                    B.timeZoneId,
                    B.isActivated,
                    B.isDeleted,
                    B.seConfigId,
                    'Blend'
                    FROM (SELECT *, MIN(`arcDate`) FROM {tablename_RepDateLocation_arc} GROUP BY `date`, `repId`) as A
                    INNER JOIN {tablename_Rep} as B USING (repId)
                    INNER JOIN {tablename_RepLocation} as C USING(repId);         
                '''.format(**self.tablename_dict)
            logger.info("Query used for insert is: {initial_insert}".format(initial_insert=initial_insert))
            other_database_op(initial_insert, self.conn_pool, return_cursor=False)

        logger.info("Creating {tablename_RPT_RepDateLocation_history_TEMP_COPY}".format(**self.tablename_dict))
        other_database_op("CREATE TABLE {tablename_RPT_RepDateLocation_history_TEMP_COPY} LIKE {tablename_RPT_RepDateLocation_history};".format(**self.tablename_dict), self.conn_pool, return_cursor=False)

        logger.info("Executing conditional insert into {tablename_RPT_RepDateLocation_history_TEMP_COPY}".format(**self.tablename_dict))
        conditional_insert = \
            '''INSERT INTO {tablename_RPT_RepDateLocation_history_TEMP_COPY}
            (repId,`date`,latitude,longitude,latLongString,maxNearDistance,maxFarDistance,dailyTravelDistanceMi,dailyTravelDistanceKm,learningRunUID,source,sourceCatagory,repTypeId, workWeekId,externalId,repName,timeZoneId,isActivated,isDeleted,seConfigId,`blend`)
                SELECT 
                repId,`date`,latitude,longitude,latLongString,maxNearDistance,maxFarDistance,dailyTravelDistanceMi,dailyTravelDistanceKm,learningRunUID,source,sourceCatagory,repTypeId, workWeekId,externalId,repName,timeZoneId,isActivated,isDeleted,seConfigId,`blend`
                FROM {tablename_RPT_RepDateLocation_history} WHERE `date` not in (SELECT DISTINCT `date` FROM {tablename_RepDateLocation});
            '''.format(**self.tablename_dict)
        logger.info("Query used for insert is: {conditional_insert}".format(conditional_insert=conditional_insert))
        other_database_op(conditional_insert, self.conn_pool, return_cursor=False)

        logger.info("Dropping old {tablename_RPT_RepDateLocation_history}".format(**self.tablename_dict))
        other_database_op("DROP TABLE {tablename_RPT_RepDateLocation_history};".format(**self.tablename_dict), self.conn_pool, return_cursor=False)

        logger.info("Renaming {tablename_RPT_RepDateLocation_history_TEMP_COPY} to {tablename_RPT_RepDateLocation_history}".format(**self.tablename_dict))
        other_database_op("RENAME TABLE {tablename_RPT_RepDateLocation_history_TEMP_COPY} TO {tablename_RPT_RepDateLocation_history};".format(**self.tablename_dict), self.conn_pool, return_cursor=False)

        if "RepDateLocation" in self.empty_table_set:
            logger.info("RepDateLocation in dse empty, skipping nightly insert")
            return

        logger.info("doing nightly insert")
        nightly_insert = \
            '''INSERT INTO {tablename_RPT_RepDateLocation_history}
            (repId,`date`,latitude,longitude,latLongString,maxNearDistance,maxFarDistance,dailyTravelDistanceMi,dailyTravelDistanceKm,learningRunUID,source,sourceCatagory,repTypeId, workWeekId,externalId,repName,timeZoneId,isActivated,isDeleted,seConfigId,`blend`)
            SELECT
                A.repId,
                A.date,
                A.latitude,
                A.longitude,
                CONCAT("Lat: ", ROUND(A.Latitude, 2), ", Long: ", ROUND(A.Longitude, 2)),
                A.maxNearDistance,
                IFNULL(A.maxFarDistance, 0),
                ROUND(A.maxNearDistance / 1609.34, 2),
                ROUND(A.maxNearDistance / 1000, 2),
                A.learningRunUID,
                A.source,
                case
  			        when A.source regexp 'calendar' then 'calendar'
  			        when A.source regexp 'history' then 'history'
  			        when A.source regexp 'territory' then 'territory'  
		        else 
  			        A.source
		        end,
                B.repTypeId,
                B.workWeekId,
                B.externalId,
                B.repName,
                B.timeZoneId,
                B.isActivated,
                B.isDeleted,
                B.seConfigId,
                'Blend'
                FROM {tablename_RepDateLocation} A
                INNER JOIN {tablename_Rep} B USING (repId);           
            '''.format(**self.tablename_dict)
        logger.info("Inserting. Query used for nightly insert is: {nightly_insert}".format(nightly_insert=nightly_insert))
        other_database_op(nightly_insert, self.conn_pool, return_cursor=False)
        logger.info("Completed update of RPT_RepDateLocation_history")


    def _update_RPT_RepDateLocationMap_history(self):
        logger.info("updating RPT_RepDateLocationMap_history")
        logger.info("First, truncating RPT_RepDateLocationMap_history")
        other_database_op("TRUNCATE TABLE {tablename_RPT_RepDateLocationMap_history};".format(**self.tablename_dict), self.conn_pool, return_cursor=False)
        initial_insert = \
            '''INSERT INTO {tablename_RPT_RepDateLocationMap_history}
            (repId,`date`,latitude,longitude,latLongString,maxNearDistance,maxFarDistance,dailyTravelDistanceMi,dailyTravelDistanceKm,displayLat,displayLong,learningRunUID,source,sourceCatagory,repTypeId,workWeekId,externalId,repName,timeZoneId,isActivated,isDeleted,seConfigId,blend,Degree)
            SELECT
                A.repId,
                A.date,
                A.latitude,
                A.longitude,
                A.latLongString,
                A.maxNearDistance,
                A.maxFarDistance,
                A.dailyTravelDistanceMi,
                A.dailyTravelDistanceKm,
                DEGREES(ASIN(SIN(RADIANS(A.latitude)) *COS((A.dailyTravelDistanceMi)/ 3959) +COS(RADIANS(A.latitude)) * SIN((A.dailyTravelDistanceMi)/ 3959)*COS(RADIANS(D.Degree)))),
                DEGREES(RADIANS(A.longitude) + ATAN2(COS((A.dailyTravelDistanceMi) / 3959)-SIN(RADIANS(A.latitude)) * SIN(RADIANS(DEGREES(ASIN(SIN(RADIANS(A.latitude)) *COS((A.dailyTravelDistanceMi)/ 3959) +COS(RADIANS(A.latitude)) * SIN((A.dailyTravelDistanceMi)/ 3959)*COS(RADIANS(D.Degree)))))) , SIN(RADIANS(D.Degree)) * SIN((A.dailyTravelDistanceMi) / 3959) * COS(RADIANS(A.latitude))))-90,
                A.learningRunUID,
                A.source,
                A.sourceCatagory,
                A.repTypeId,
                A.workWeekId,
                A.externalId,
                A.repName,
                A.timeZoneId,
                A.isActivated,
                A.isDeleted,
                A.seConfigId,
                D.blend,
                D.Degree
                FROM {tablename_RPT_RepDateLocation_history} as A
                LEFT JOIN {tablename_RPT_Degrees} AS D ON D.blend = 'Blend';
            '''.format(**self.tablename_dict)
        logger.info("Inserting. Query used for insert is: {initial_insert}".format(initial_insert=initial_insert))
        other_database_op(initial_insert, self.conn_pool, return_cursor=False)
        logger.info("Completed update of RPT_RepDateLocationMap_history")

    def run(self):
        logger.info("Start Running ReportingTableUpdateScripts")
        self._update_RPT_RepDateLocation_history()
        self._update_RPT_RepDateLocationMap_history()
        logger.info("Finish Running ReportingTableUpdateScripts")

