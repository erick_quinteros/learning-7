#!/bin/bash

################################################################################
# This shell script runs and automatically creates an EMR instance in the      #
# specified region, with the specified cluster name with speficied AMI.        #
#                                                                              #
#                   MODULE: Learning Fusion                                    #
#                   CREATED BY: Anwar Shaikh                                   #
#                   CREATED AT: 18-SEP-2019                                    #
#                   UPDATED BY: Anwar Shaikh                                   #
#                   UPDATED AT: 25-SEP-2019                                    #
################################################################################

# Check the number of Arguments
if [[ $# -lt 3 ]]; then
    echo "Illegal number of parameters to the script"
    exit 2
fi

# Default Values
DEFAULT_KEY="learning-team-dev"

# Parse the Arguments
SCIRPT_VERSION="1.0"
REGION=$1
IMAGE_ID=$2
CLUSTER_NAME=$3
KEY_NAME=${4:-$DEFAULT_KEY}


# Setup AWS Credentials
export AWS_ACCESS_KEY_ID="AKIAXRUJHASADTAL4LBE"
export AWS_SECRET_ACCESS_KEY="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"
export AWS_DEFAULT_REGION="$REGION"


# Create the cluster
aws emr create-cluster --name "$CLUSTER_NAME" --custom-ami-id $IMAGE_ID \
 --ebs-root-volume-size 20 --release-label emr-5.16.0 --use-default-roles --instance-count 2 \
 --instance-type m3.xlarge --ec2-attributes "{\"KeyName\": \"$KEY_NAME\"}"
