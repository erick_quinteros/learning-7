# Unit Test Setting up with "testthat" in R Studio


1. Install "testthat package" and load testthat package

        install.packages('testthat')
        library('testthat')


2. Establish the test directory structure
    * create the test main directory

            mkdir './tests/'
            cd './tests/'

    * create testthat.R file and testthat subdirectory (for package testing only)

            vim testthat.R
                library(testthat)
                test_check("messageSequence")
            mkdir './testthat'
            cd './testthat'

3. Write test codes  
   The test code file name should start with **"test_"** and usually should have the following structute

        vim test_example.r
            context('testing example') # describe of the test codes
            test_that("first example", {
                expect_equal(length(12),1)
                expect_equal(length(12),2)
            test_that('second example', {
                a <- b <- 1:3
                names(b) <- letters[1:3]
                expect_equivalent(a, b)
            })

4. Run the tests (in R studio)
    * Run all the tests in the folder
        
            test_dir('./tests/')

    * Run the test file one by one

            test_file('./test/testthat/test_example.R')


*For more information about the "testthat" package, check: https://cran.r-project.org/web/packages/testthat/testthat.pdf*