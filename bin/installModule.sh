#!/bin/bash
#
# aktana-learning Run script for running an R script in R learning module.
#
# description: Learning modules are written in R. The script is used
#              to run appropriate scripts in R packages
#
# created by : satya.dhanushkodi@aktana.com
# modified by : marc.cohen@aktana.com 2/7/2017
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#

# Set the learning home location if not set already
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
fi
LOG_DIR=$LEARNING_HOME/../logs
LIB_DIR=$LEARNING_HOME/../library
DATA_DIR=$LEARNING_HOME/../data
MODULE="$1"
LOGNAME=$MODULE.installPackages.stdout
RSCRIPT="$2"
PYSCRIPT="$3"

if [ -z "$1" ] ; then
  echo "MODULE name should be specified as arg1"
  exit 1
fi

if [ -z "$2" ] ; then
  echo "SCRIPT name should be specified as arg2"
  exit 1
fi

LOG_DIR_PARAM='"'"$LOG_DIR"'"'
DATA_DIR_PARAM='"'"$DATA_DIR"'"'
HOME_DIR_PARAM='"'"$LEARNING_HOME"'"'
LIB_DIR_PARAM='"'"$LIB_DIR"'"'

RCMD="R CMD BATCH --no-save --no-restore "
RARGS="--args logdir=$LOG_DIR_PARAM datadir=$DATA_DIR_PARAM homedir=$HOME_DIR_PARAM libdir=$LIB_DIR_PARAM"
RFILES=" $LEARNING_HOME/$MODULE/$RSCRIPT $LOG_DIR/$LOGNAME"

RFULLCMD="$RCMD '$RARGS ' $RFILES"

# Make sure log directory exists
if [ ! -d $LOG_DIR ]; then
    mkdir $LOG_DIR
fi

# Make sure library directory exists
if [ ! -d $LIB_DIR ]; then
    mkdir $LIB_DIR
fi

# Make sure data directory exists
if [ ! -d $DATA_DIR ]; then
    mkdir $DATA_DIR
fi

# Verify if the module is in place.
if [ ! -f "$LEARNING_HOME/$MODULE/$RSCRIPT" ] ;
then
  echo "No $LEARNING_HOME/$MODULE/$RSCRIPT, not install r packages"
else
  echo "install r packages $MODULE/$RSCRIPT"
  echo $RFULLCMD
  eval $RFULLCMD
  echo "done install r packages $MODULE/$RSCRIPT"
fi

# install python package if there is requirement files
PYCMD="python3 -m pip install -r"
PYFILES=" $LEARNING_HOME/$MODULE/$PYSCRIPT"
PYFULLCMD="$PYCMD $PYFILES >> $LOG_DIR/$LOGNAME"

# Verify if the module is in place.
if [ ! -f "$LEARNING_HOME/$MODULE/$PYSCRIPT" ] ;
then
  echo "No $LEARNING_HOME/$MODULE/$PYSCIPT file, not install python packages"
else
  echo "install python packages $MODULE/$PYSCRIPT"
  echo $PYFULLCMD
  eval $PYFULLCMD
  echo "done install python packages $MODULE/$PYSCRIPT"
fi

echo "done install $MODULE"
